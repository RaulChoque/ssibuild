package com.izzysoft.ssibuild.Model;

import javax.persistence.Entity;

@Entity
public class CargoContacto extends ModelBase {
    private String descripcion;
    private int jerarquia;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getJerarquia() {
        return jerarquia;
    }

    public void setJerarquia(int jerarquia) {
        this.jerarquia = jerarquia;
    }
}

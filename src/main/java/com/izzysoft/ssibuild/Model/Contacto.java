package com.izzysoft.ssibuild.Model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Contacto extends ModelBase {
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String primerNombre;
    private String segundoNombre;
    private String telefono;
    private String celular;
    @OneToOne(optional = false, targetEntity = Proveedor.class)
    private Proveedor proveedor;
    @OneToOne(optional = false, targetEntity = EstadoContacto.class)
    private EstadoContacto estadoContacto;
    @OneToOne(optional = false, targetEntity = CargoContacto.class)
    private CargoContacto cargoContacto;

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public EstadoContacto getEstadoContacto() {
        return estadoContacto;
    }

    public void setEstadoContacto(EstadoContacto estadoContacto) {
        this.estadoContacto = estadoContacto;
    }

    public CargoContacto getCargoContacto() {
        return cargoContacto;
    }

    public void setCargoContacto(CargoContacto cargoContacto) {
        this.cargoContacto = cargoContacto;
    }
}

package com.izzysoft.ssibuild.Model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.Date;

@Entity
public class Equipo extends ModelBase {
    private String descripcion;
    private Date fechaAdquisicion;
    private Date fechaBaja;
    private String codigoIndustria;
    private String garantia;
    private Double valorAdquisicion;
    @OneToOne(optional = false, targetEntity = UnidadGarantia.class)
    private UnidadGarantia unidadGarantia;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaAdquisicion() {
        return fechaAdquisicion;
    }

    public void setFechaAdquisicion(Date fechaAdquisicion) {
        this.fechaAdquisicion = fechaAdquisicion;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public String getCodigoIndustria() {
        return codigoIndustria;
    }

    public void setCodigoIndustria(String codigoIndustria) {
        this.codigoIndustria = codigoIndustria;
    }

    public String getGarantia() {
        return garantia;
    }

    public void setGarantia(String garantia) {
        this.garantia = garantia;
    }

    public Double getValorAdquisicion() {
        return valorAdquisicion;
    }

    public void setValorAdquisicion(Double valorAdquisicion) {
        this.valorAdquisicion = valorAdquisicion;
    }

    public UnidadGarantia getUnidadGarantia() {
        return unidadGarantia;
    }

    public void setUnidadGarantia(UnidadGarantia unidadGarantia) {
        this.unidadGarantia = unidadGarantia;
    }
}

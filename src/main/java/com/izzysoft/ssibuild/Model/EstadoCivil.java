/**
 * @autor: EstadoCivil
 **/
package com.izzysoft.ssibuild.Model;

import javax.persistence.Entity;

@Entity
public class EstadoCivil extends ModelBase {

    private String descripcion;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}

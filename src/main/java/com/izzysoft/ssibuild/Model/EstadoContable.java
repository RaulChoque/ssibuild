package com.izzysoft.ssibuild.Model;

import javax.persistence.Entity;

@Entity
public class EstadoContable extends ModelBase {
    private char codigo;
    private int gestion;
    private int periodo;
    private String descripcion;
    private Double depreciacionAcumulada;
    private Double actualizacionDepreciacionAcumulada;
    private Double depreciacionGestion;
    private Double actualizacionDepreciacionGestion;
    private Double actualizacionValor;
    private Double valorNeto;

    public char getCodigo() {
        return codigo;
    }

    public void setCodigo(char codigo) {
        this.codigo = codigo;
    }

    public int getGestion() {
        return gestion;
    }

    public void setGestion(int gestion) {
        this.gestion = gestion;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getDepreciacionAcumulada() {
        return depreciacionAcumulada;
    }

    public void setDepreciacionAcumulada(Double depreciacionAcumulada) {
        this.depreciacionAcumulada = depreciacionAcumulada;
    }

    public Double getActualizacionDepreciacionAcumulada() {
        return actualizacionDepreciacionAcumulada;
    }

    public void setActualizacionDepreciacionAcumulada(Double actualizacionDepreciacionAcumulada) {
        this.actualizacionDepreciacionAcumulada = actualizacionDepreciacionAcumulada;
    }

    public Double getDepreciacionGestion() {
        return depreciacionGestion;
    }

    public void setDepreciacionGestion(Double depreciacionGestion) {
        this.depreciacionGestion = depreciacionGestion;
    }

    public Double getActualizacionDepreciacionGestion() {
        return actualizacionDepreciacionGestion;
    }

    public void setActualizacionDepreciacionGestion(Double actualizacionDepreciacionGestion) {
        this.actualizacionDepreciacionGestion = actualizacionDepreciacionGestion;
    }

    public Double getActualizacionValor() {
        return actualizacionValor;
    }

    public void setActualizacionValor(Double actualizacionValor) {
        this.actualizacionValor = actualizacionValor;
    }

    public Double getValorNeto() {
        return valorNeto;
    }

    public void setValorNeto(Double valorNeto) {
        this.valorNeto = valorNeto;
    }
}

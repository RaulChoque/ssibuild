package com.izzysoft.ssibuild.Model;

import javax.persistence.Entity;
import java.util.Date;

@Entity
public class EstadoFisico extends ModelBase {
    private String descripcion;
    private Date fechaRegistro;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
}

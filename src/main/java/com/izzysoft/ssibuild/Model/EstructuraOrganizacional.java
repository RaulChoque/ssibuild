/**
 * @autor: Raúl Choque
 **/
package com.izzysoft.ssibuild.Model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity

public class EstructuraOrganizacional extends ModelBase {
    private Integer dependencia;
    private Integer nivelJerarquia;
    private String description;
    private Character estado;
    @OneToOne(optional = false)
    private Empresa empresa;
    @OneToOne(optional = false)
    private Personal personal;

    public Personal getPersonal() {
        return personal;
    }

    public void setPersonal(Personal personal) {
        this.personal = personal;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Integer getDependencia() {
        return dependencia;
    }

    public void setDependencia(Integer dependencia) {
        this.dependencia = dependencia;
    }
    public Integer getNivelJerarquia() {
        return nivelJerarquia;
    }

    public void setNivelJerarquia(Integer nivelJerarquia) {
        this.nivelJerarquia = nivelJerarquia;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}

/**
 * @autor: FichaHistorica
 **/
package com.izzysoft.ssibuild.Model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.Date;

@Entity
public class FichaHistorica extends ModelBase {

    @OneToOne(optional = false)
    private HistorialPersonal historialPersonal;
    private Date fechaIncorporacion;
    private Date fechaBaja;
    private String motivoBaja;

    public HistorialPersonal getHistorialPersonal() {
        return historialPersonal;
    }

    public void setHistorialPersonal(HistorialPersonal historialPersonal) {
        this.historialPersonal = historialPersonal;
    }

    public String getMotivoBaja() {
        return motivoBaja;
    }

    public void setMotivoBaja(String motivoBaja) {
        this.motivoBaja = motivoBaja;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public Date getFechaIncorporacion() {
        return fechaIncorporacion;
    }

    public void setFechaIncorporacion(Date fechaIncorporacion) {
        this.fechaIncorporacion = fechaIncorporacion;
    }
}

/**
 * @autor: HistorialPersonal
 **/
package com.izzysoft.ssibuild.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class HistorialPersonal extends ModelBase {

    @OneToOne(optional = false)
    private Personal personal;
    @OneToMany(mappedBy = "historialPersonal", fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    private List<FichaHistorica> fichaHistorica = new ArrayList<>();

    public Personal getPersonal() {
        return personal;
    }

    public void setPersonal(Personal personal) {
        this.personal = personal;
    }

    public List<FichaHistorica> getFichaHistorica() {
        return fichaHistorica;
    }

    public void setFichaHistorica(List<FichaHistorica> fichaHistorica) {
        this.fichaHistorica = fichaHistorica;
    }
}

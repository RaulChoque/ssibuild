package com.izzysoft.ssibuild.Model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Maquinaria extends ModelBase {
    @OneToOne(optional = false, targetEntity = TipoActivo.class)
    private TipoActivo tipoActivo;
    @OneToOne(optional = false, targetEntity = EstadoContable.class)
    private EstadoContable estadoContable;
    @OneToOne(optional = false, targetEntity = EstadoFisico.class)
    private EstadoFisico estadoFisico;
    @ManyToMany(fetch = FetchType.EAGER, targetEntity = Proveedor.class)
    private Set<Proveedor> proveedores = new HashSet<>();

    public TipoActivo getTipoActivo() {
        return tipoActivo;
    }

    public void setTipoActivo(TipoActivo tipoActivo) {
        this.tipoActivo = tipoActivo;
    }

    public EstadoContable getEstadoContable() {
        return estadoContable;
    }

    public void setEstadoContable(EstadoContable estadoContable) {
        this.estadoContable = estadoContable;
    }

    public EstadoFisico getEstadoFisico() {
        return estadoFisico;
    }

    public void setEstadoFisico(EstadoFisico estadoFisico) {
        this.estadoFisico = estadoFisico;
    }

    public Set<Proveedor> getProveedores() {
        return proveedores;
    }

    public void setProveedores(Set<Proveedor> proveedores) {
        this.proveedores = proveedores;
    }
}

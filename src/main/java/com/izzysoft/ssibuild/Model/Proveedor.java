package com.izzysoft.ssibuild.Model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Proveedor extends ModelBase{
    private char estado;
    private String nombreComercial;
    private String razonSocial;
    @OneToOne(optional = false, targetEntity = TipoProveedor.class)
    private TipoProveedor tipoProveedor;
    @OneToOne(optional = false, targetEntity = TipoDocumento.class)
    private TipoDocumento tipoDocumento;
    @OneToMany(mappedBy = "proveedor", fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    private Set<Contacto> contactos = new HashSet<>();
    @ManyToMany(mappedBy = "proveedores", fetch = FetchType.EAGER, targetEntity = Maquinaria.class)
    private Set<Maquinaria> maquinarias = new HashSet<>();
    //@ManyToMany(mappedBy = "proveedores", fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    //private Set<Equipo> equipos = new HashSet<>();
    private long numeroDocumento;
    private String direccion;
    private int diasPlazo;
    private String telefono;
    private String celular;
    private String observacion;

    public char getEstado() {
        return estado;
    }

    public void setEstado(char estado) {
        this.estado = estado;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public TipoProveedor getTipoProveedor() {
        return tipoProveedor;
    }

    public void setTipoProveedor(TipoProveedor tipoProveedor) {
        this.tipoProveedor = tipoProveedor;
    }

    public TipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public long getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(long numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getDiasPlazo() {
        return diasPlazo;
    }

    public void setDiasPlazo(int diasPlazo) {
        this.diasPlazo = diasPlazo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
}

package com.izzysoft.ssibuild.Model;

import javax.persistence.Entity;

@Entity
public class TipoActivo extends ModelBase {
    private String unidaMedida;
    private int vidaUtil;

    public String getUnidaMedida() {
        return unidaMedida;
    }

    public void setUnidaMedida(String unidaMedida) {
        this.unidaMedida = unidaMedida;
    }

    public int getVidaUtil() {
        return vidaUtil;
    }

    public void setVidaUtil(int vidaUtil) {
        this.vidaUtil = vidaUtil;
    }
}

package com.izzysoft.ssibuild.Model;

import javax.persistence.Entity;

@Entity
public class UnidadGarantia extends ModelBase {
    private String descripcion;
    private String alcance;
    private String limite;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAlcance() {
        return alcance;
    }

    public void setAlcance(String alcance) {
        this.alcance = alcance;
    }

    public String getLimite() {
        return limite;
    }

    public void setLimite(String limite) {
        this.limite = limite;
    }
}

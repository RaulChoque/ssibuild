/**
 * @autor: Raúl Choque
 **/
package com.izzysoft.ssibuild;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing
public class PersistenceConfig {
}

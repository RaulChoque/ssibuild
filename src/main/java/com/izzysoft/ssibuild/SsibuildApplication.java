package com.izzysoft.ssibuild;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsibuildApplication {

    public static void main(String[] args) {
        SpringApplication.run(SsibuildApplication.class, args);
    }
}

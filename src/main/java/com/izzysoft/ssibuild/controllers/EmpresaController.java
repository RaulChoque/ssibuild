/**
 * @autor: Raúl Choque
 **/
package com.izzysoft.ssibuild.controllers;

import com.izzysoft.ssibuild.Model.Empresa;
import com.izzysoft.ssibuild.services.EmpresaService;
import org.springframework.stereotype.Controller;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


@Path("/empresas")
@Produces("application/json")
@Controller

public class EmpresaController {
    private EmpresaService service;

    public EmpresaController(EmpresaService service) {
        this.service = service;
    }

    @GET
    public Response getEmpresas() {
        List<Empresa> empresaList = service.getAllEmpresas();
        Response.ResponseBuilder responseBuilder = Response.ok(empresaList);
        addCorsHeader(responseBuilder);
        return responseBuilder.build();
    }
    @GET
    @Path("/{id}")
    public Response getEmpresaById(@PathParam("id") @NotNull Long id){
        Empresa empresa = service.findById(id);
        Response.ResponseBuilder responseBuilder = Response.ok(empresa);
        addCorsHeader(responseBuilder);
        return responseBuilder.build();

    }

    @POST
    public Response createEmpresa(Empresa empresa){

        Empresa newEmpresa = service.addEmpresa(empresa);
        Response.ResponseBuilder responseBuilder = Response.ok(newEmpresa);
        addCorsHeader(responseBuilder);
        return responseBuilder.build();
    }

    @PUT
    public Response updateEmpresa(Empresa empresa){
        Empresa updateEmpresa = service.save(empresa);
        Response.ResponseBuilder responseBuilder = Response.ok(updateEmpresa);
        addCorsHeader(responseBuilder);
        return responseBuilder.build();
    }
    @DELETE
    @Path("/delete/{id}")
    public Response deleteEmpresaById(@PathParam("id") @NotNull Long id){
        service.deleteById(id);
        Response.ResponseBuilder responseBuilder = Response.ok();
        addCorsHeader(responseBuilder);
        return responseBuilder.build();

    }

    private void addCorsHeader(Response.ResponseBuilder responseBuilder) {
        responseBuilder.header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .header("Access-Control-Allow-Headers",
                        "Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Origin, Accept, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    }
}


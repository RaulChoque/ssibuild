/**
 * @author: raulchoque
 **/
package com.izzysoft.ssibuild.repositories;

import org.springframework.data.repository.CrudRepository;
import com.izzysoft.ssibuild.Model.Empresa;

public interface EmpresaRepository extends CrudRepository<Empresa, Long> {
}
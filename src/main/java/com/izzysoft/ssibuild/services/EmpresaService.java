/**
 * @author: raulchoque
 **/
package com.izzysoft.ssibuild.services;

import com.izzysoft.ssibuild.Model.Empresa;

import java.util.List;

public interface EmpresaService extends GenericService<Empresa> {

    List<Empresa> getAllEmpresas();
    Empresa addEmpresa(Empresa empresa);
}

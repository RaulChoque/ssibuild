/**
 * @autor: raulchoque :)
 **/
package com.izzysoft.ssibuild.services;

import com.izzysoft.ssibuild.Model.Empresa;
import com.izzysoft.ssibuild.repositories.EmpresaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service

public class EmpresaServiceImpl extends GenericServiceImpl<Empresa> implements EmpresaService {

    private EmpresaRepository repositoryEmpresa;

    public EmpresaServiceImpl(EmpresaRepository repositoryEmpresa) {
        this.repositoryEmpresa = repositoryEmpresa;
    }

    @Override
    public List<Empresa> getAllEmpresas(){
        List <Empresa> listEmpresas = new ArrayList<>();
        repositoryEmpresa.findAll().forEach(listEmpresas::add);
        return listEmpresas;
    }
    @Override
    public Empresa addEmpresa(Empresa empresa) {
        Empresa nuevaEmpresa = copyDataEmpresa(empresa);

        return repositoryEmpresa.save(nuevaEmpresa);
    }

    private Empresa copyDataEmpresa(Empresa empresa) {
        Empresa nuevaEmpresa = new Empresa();
        nuevaEmpresa.setRazonSocial(empresa.getRazonSocial());
        nuevaEmpresa.setNombreComercial(empresa.getNombreComercial());
        nuevaEmpresa.setUbicacion(empresa.getUbicacion());
        nuevaEmpresa.setNit(empresa.getNit());
        return nuevaEmpresa;
    }


    @Override
    protected CrudRepository<Empresa, Long> getRepository() {
        return repositoryEmpresa;
    }


}
